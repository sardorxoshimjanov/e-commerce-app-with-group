package uz.pdp.ecommerceapp.projection;
//Sevinch Abdisattorova 04/11/2022 10:39 PM


import java.util.UUID;

public interface CustomCharacteristicValue {

    UUID getId();

    String getValue();
}
