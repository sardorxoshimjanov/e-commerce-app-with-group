package uz.pdp.ecommerceapp.projection;

import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.UUID;

public interface ProductWithCharProjection {
    UUID getProductId();

    String getProductNameCharacteristic();

    String getCharacteristicCategoryName();

    @Value("#{@characteristicValueRepository.getCharacterNameAndValueNameWithCharacterIdAndValueId(target.productId)}")
    List<CharacterAndValueNameProjection> getCharacterAndValue();


}
