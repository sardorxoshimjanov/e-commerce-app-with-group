package uz.pdp.ecommerceapp.projection;
//Sevinch Abdisattorova 04/11/2022 10:39 PM


import uz.pdp.ecommerceapp.model.enums.Status;

import java.time.LocalDateTime;
import java.util.UUID;

public interface CustomOrder {

    UUID getId();

    UUID getUserId();

    String getUserFullName();

    Integer getSerialNumber();

    Status getStatus();

    LocalDateTime getCreatedAt();
}
