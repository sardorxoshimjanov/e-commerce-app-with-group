package uz.pdp.ecommerceapp.projection;
//Sevinch Abdisattorova 04/12/2022 11:49 AM


import java.util.UUID;

public interface CustomCharacteristicCategory {

    UUID getId();

    String getName();
}
