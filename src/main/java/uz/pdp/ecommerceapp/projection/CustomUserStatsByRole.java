package uz.pdp.ecommerceapp.projection;
//Sevinch Abdisattorova 04/11/2022 10:39 PM


public interface CustomUserStatsByRole {

    String getRole();

    Integer getCount();
}
