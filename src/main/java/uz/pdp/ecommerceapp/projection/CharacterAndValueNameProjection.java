package uz.pdp.ecommerceapp.projection;

public interface CharacterAndValueNameProjection {
    String getCharacterName();
    String getValueName();
}
