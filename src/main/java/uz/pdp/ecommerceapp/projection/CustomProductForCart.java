package uz.pdp.ecommerceapp.projection;
//Sevinch Abdisattorova 04/12/2022 12:34 PM

import java.util.UUID;

public interface CustomProductForCart {

    UUID getId();

    UUID getPhotoId();

    String getProductName();

    Double getPrice();

}
