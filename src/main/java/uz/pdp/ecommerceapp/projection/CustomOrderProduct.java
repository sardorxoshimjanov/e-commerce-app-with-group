package uz.pdp.ecommerceapp.projection;
//Sevinch Abdisattorova 04/11/2022 10:39 PM


import uz.pdp.ecommerceapp.model.Product;
import uz.pdp.ecommerceapp.model.enums.Status;

import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.UUID;

public interface CustomOrderProduct {

    UUID getId();

    Integer getQuantity();

    Double getTotalPrice();

    UUID getProductId();

    String getProductName();
}
