package uz.pdp.ecommerceapp.projection;

import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.UUID;

public interface ProductByIdProjection {
    UUID getProductId();

    String getProductName();

    Double getProductPrice();

    String getDescription();

    @Value("#{@attachmentRepository.getAttachmentByProductId(target.productId)}")
    List<UUID> getAttachmentIds();
}
