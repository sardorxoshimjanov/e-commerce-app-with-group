package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 2:06 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.enums.*;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "orders")
public class Order extends AbsEntity {

    @ManyToOne
    User user;

    @Column()
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer serialNumber;

    @Enumerated(EnumType.STRING)
    Status status;

    public Order(User user, Status status) {
        this.user = user;
        this.status = status;
    }
}
