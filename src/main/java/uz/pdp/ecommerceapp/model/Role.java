package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 03/14/2022 6:58 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.enums.RoleEnum;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "roles")
@PackagePrivate
public class Role extends AbsEntity {

    @Enumerated(EnumType.STRING)
    RoleEnum role;

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinTable(
            name = "roles_permissions",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "permission_id")})
    Set<Permission> permissions;
}
