package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 2:19 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "values")
public class Value extends AbsEntity {
    String value;
}
