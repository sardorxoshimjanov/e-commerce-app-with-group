package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 1:56 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "attachment_contents")
public class AttachmentContent extends AbsEntity {
    @OneToOne(cascade = CascadeType.MERGE)
    Attachment attachment;
    byte [] data;
}
