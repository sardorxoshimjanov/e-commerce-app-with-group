package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 2:12 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "brands")
public class Brand extends AbsEntity {
    String name;

}
