package uz.pdp.ecommerceapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

// Bahodir Hasanov 4/11/2022 2:48 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "favorites_products")
@PackagePrivate
public class FavoriteProduct extends AbsEntity {

    @ManyToOne
    User user;

    @ManyToOne
    Product product;

}
