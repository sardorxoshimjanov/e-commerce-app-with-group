package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/13/2022 11:08 AM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity
public class PeriodicPayment extends AbsEntity {

    @ManyToOne
    User user;

    Integer months;

    @ManyToOne
    Order order;

}
