package uz.pdp.ecommerceapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "order_items")
public class OrderProduct extends AbsEntity {

    Integer quantity;

    Double totalPrice;

    @ManyToOne(cascade = CascadeType.MERGE)
    Order order;

    @ManyToOne
    Product product;
}
