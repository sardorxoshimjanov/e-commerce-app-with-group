package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 1:56 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "additional_in_months")
public class AdditionalInMonth extends AbsEntity {

    Integer monthNumber;

    double percentage;

}
