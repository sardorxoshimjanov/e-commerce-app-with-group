package uz.pdp.ecommerceapp.model.enums;

public enum Status {
    NEW,
    PURCHASED
}
