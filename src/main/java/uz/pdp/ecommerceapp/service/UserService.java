package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/13/2022 11:51 AM


import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.UserDto;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.repository.UserRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    @Value("${ACCOUNT_SID}")
    String accountSid;

    @Value("${AUTH_TOKEN}")
    String token;

    @Value("${TWILIO_NUMBER}")
    String twilioPhoneNumber;

    public ResponseEntity<?> saveCurrentUser(UserDto userDto,
                                             HttpServletResponse response) {

        Twilio.init(accountSid, token);

        String phoneNumber = userDto.getPhoneNumber();
        User userByPhoneNum = userRepository.findByPhoneNumber(phoneNumber);

        if (userByPhoneNum == null) {
            userByPhoneNum = new User(userDto.getFullName(), phoneNumber);
        }
        if (userDto.getFullName() != null)
            userByPhoneNum.setFullName(userDto.getFullName());

        Integer confirmCode = (int) Math.floor(Math.random() * 8999 + 1000);
        Message message = Message.creator(
                new PhoneNumber(phoneNumber),
                new PhoneNumber(twilioPhoneNumber),
                "Hi," + userByPhoneNum.getFullName() + "\n" + "Confirmation code for online-shop " + confirmCode).create();

        userByPhoneNum.setConfirmationCode(confirmCode);
        userRepository.save(userByPhoneNum);
        Cookie cookie = new Cookie("phoneNumber", phoneNumber);
        cookie.setPath("/api");

        response.addCookie(cookie);

        return ResponseEntity.ok("We have send confirmation code to your phone number!");
    }
}
