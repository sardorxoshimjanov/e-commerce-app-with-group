package uz.pdp.ecommerceapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.model.Brand;
import uz.pdp.ecommerceapp.model.FirstPaymentPercentage;
import uz.pdp.ecommerceapp.repository.BrandRepository;
import uz.pdp.ecommerceapp.repository.FirstPaymentPercentageRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FirstPaymentPercentageService {

    private final FirstPaymentPercentageRepository firstPaymentPercentageRepository;

    public ResponseEntity<?> getAllFirstPaymentPercentages() {
        List<FirstPaymentPercentage> paymentPercentageList = firstPaymentPercentageRepository.findAll();
        return ResponseEntity.ok(paymentPercentageList);
    }

    public ResponseEntity<?> getFirstPaymentPercentageById(UUID id) {
        Optional<FirstPaymentPercentage> optionalFirstPaymentPercentage = firstPaymentPercentageRepository.findById(id);
        return ResponseEntity.status(optionalFirstPaymentPercentage.isPresent()?200:404).body(optionalFirstPaymentPercentage.get());
    }

    public ResponseEntity<?> editFirstPaymentPercentage(UUID id, FirstPaymentPercentage firstPaymentPercentage) {
        Optional<FirstPaymentPercentage> optionalFirstPaymentPercentage = firstPaymentPercentageRepository.findById(id);
        if (!optionalFirstPaymentPercentage.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        FirstPaymentPercentage editingPaymentPercentage = optionalFirstPaymentPercentage.get();
        editingPaymentPercentage.setPercentage(firstPaymentPercentage.getPercentage());
        FirstPaymentPercentage paymentPercentage = firstPaymentPercentageRepository.save(editingPaymentPercentage);
        return ResponseEntity.ok(paymentPercentage);
    }

    public ResponseEntity<?> addFirstPaymentPercentage(FirstPaymentPercentage firstPaymentPercentage) {
        FirstPaymentPercentage paymentPercentage = firstPaymentPercentageRepository.save(firstPaymentPercentage);
        return ResponseEntity.ok(paymentPercentage);
    }

    public ResponseEntity<?> deleteFirstPaymentPercentage(UUID id) {
        try {
            firstPaymentPercentageRepository.deleteById(id);
            return ResponseEntity.ok("successfully deleted");
        } catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.notFound().build();
    }
}
