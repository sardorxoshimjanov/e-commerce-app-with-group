package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/13/2022 5:20 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.CartItem;
import uz.pdp.ecommerceapp.model.Order;
import uz.pdp.ecommerceapp.model.OrderProduct;
import uz.pdp.ecommerceapp.model.Product;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.model.enums.Status;
import uz.pdp.ecommerceapp.projection.CustomOrder;
import uz.pdp.ecommerceapp.repository.OrderProductRepository;
import uz.pdp.ecommerceapp.repository.OrderRepository;
import uz.pdp.ecommerceapp.repository.ProductRepository;
import uz.pdp.ecommerceapp.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderProductRepository orderProductRepository;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;


    public void createNewOrderFromProductsInTheCart(List<CartItem> cartItemList, User user) {

        Integer lastSerialNumberOfOrder = orderRepository.findLastSerialNumberOfOrder();
        Integer serialNumber = lastSerialNumberOfOrder == null ? 1 : lastSerialNumberOfOrder + 1;
        Order order = new Order(user, serialNumber, Status.NEW);
        List<OrderProduct> orderProducts = new ArrayList<>();
        for (CartItem cartItem : cartItemList) {
            Product product = productRepository.findById(cartItem.getProduct().getId()).get();
            double totalPrice = cartItem.getProduct().getPrice() * cartItem.getQuantity();
            OrderProduct orderProduct = new OrderProduct(cartItem.getQuantity(), totalPrice, order, product);
            orderProducts.add(orderProduct);
        }
        orderProductRepository.saveAll(orderProducts);
    }


    public void changeOrderStatusToPurchased(Order order) {
        order.setStatus(Status.PURCHASED);
        orderRepository.save(order);
    }


    public ResponseEntity<?> getAllOrders() {
        List<CustomOrder> allOrders = orderRepository.getAllOrders();
        return ResponseEntity.ok(allOrders);
    }


    public ResponseEntity<?> getAllNewOrders() {
        List<CustomOrder> allNewOrders = orderRepository.getAllNewOrders();
        return ResponseEntity.ok(allNewOrders);
    }


    public ResponseEntity<?> getOrdersOfUser() {
        // TODO: 04/13/2022  get user from cookie
        User user = userRepository.findByEmail("sardorxoshimjanov");
        List<CustomOrder> allOrdersOfUser = orderRepository.getAllOrdersOfUser(user.getId());
        return ResponseEntity.ok(allOrdersOfUser);
    }
}
