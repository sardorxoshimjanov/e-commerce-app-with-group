package uz.pdp.ecommerceapp.service;//@AllArgsConstructor

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.model.Category;
import uz.pdp.ecommerceapp.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

//@NoArgsConstructor
//@Data
//@Entity
//import static uz.sardor.Main.*;
//Sardor {4/12/2022}{ 4:02 AM}
@RequiredArgsConstructor
@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public ResponseEntity<?> addCategory(Category newCategory) {
        List<Category> categoryList = categoryRepository.findAll();
        for (Category category : categoryList) {
            if (category.getName().equals(newCategory.getName())) {
                return new ResponseEntity<>("Category with this name already exists!", HttpStatus.CONFLICT);
            }
        }
        categoryRepository.save(newCategory);
        return new ResponseEntity<>("Successfully saved!", HttpStatus.CREATED);
    }

    public ResponseEntity<?> getMainCategories() {
        List<Category> categories = categoryRepository.getMainCategory();
        if (categories.isEmpty()) {
            return new ResponseEntity<>("Categories not found!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(categories, HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> getCategoriesByParentCategoryId(UUID categoryId) {

        List<Category> categoryByParentCategoryId = categoryRepository.getCategoryByParentCategoryId(categoryId);
        return ResponseEntity.ok(categoryByParentCategoryId);
    }


    public ResponseEntity<?> getCategoryById(UUID categoryId) {

        Optional<Category> optionalCategory = categoryRepository.findById(categoryId);
        if (optionalCategory.isPresent()) {
            return new ResponseEntity<>(optionalCategory, HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>("Categories not found!", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> deleteCategoryById(UUID id) {

        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (!categoryOptional.isPresent()) {
            return new ResponseEntity<>("Not deleted!", HttpStatus.NOT_FOUND);
        }
        try {
            categoryRepository.deleteById(id);
        } catch (Exception ignored) {
        }
        return new ResponseEntity<>("Successfully deleted", HttpStatus.NO_CONTENT);

    }

    public ResponseEntity<?> updateCategoryById(Category category, UUID id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (!optionalCategory.isPresent()) {
            return new ResponseEntity<>("Category not found!", HttpStatus.NOT_FOUND);
        }
        optionalCategory.get().setParentCategory(category.getParentCategory());
        optionalCategory.get().setName(category.getName());
        categoryRepository.save(optionalCategory.get());
        return new ResponseEntity<>("Successfully updated!", HttpStatus.OK);
    }


}
