package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/12/2022 12:34 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.Cart;
import uz.pdp.ecommerceapp.dto.CartItem;
import uz.pdp.ecommerceapp.dto.CartItemDto;
import uz.pdp.ecommerceapp.projection.CustomProductForCart;
import uz.pdp.ecommerceapp.repository.ProductRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Service
@RequiredArgsConstructor
public class CartService {

    private final ProductRepository productRepository;


    public ResponseEntity<?> addProductToCart(CartItemDto cartItemDto,
                                              HttpServletRequest request,
                                              HttpServletResponse response) {
        try {
            productRepository.findProductById(cartItemDto.getProductId());
            int quantity = cartItemDto.getQuantity() == null ? 1 : cartItemDto.getQuantity();
            final String[] cartItemStr = {""};
            Map<String, Integer> map = new LinkedHashMap<>();

            if (request.getCookies() != null) {
                getCartItemsMap(request, map);
            }
            map.put(cartItemDto.getProductId().toString(), quantity);
            map.forEach((key, value) -> cartItemStr[0] += key + "=" + value + "&");
            Cookie cookie = new Cookie("cartItems", cartItemStr[0]);
            //        cookie.setMaxAge(0);
            response.addCookie(cookie);
            return new ResponseEntity<>("Successfully added!", HttpStatus.OK);
        } catch (Exception ignored) {
        }
        return new ResponseEntity<>("No product found with this id", HttpStatus.CONFLICT);

    }


    public ResponseEntity<?> getCartItems(
            HttpServletRequest request) {

        Map<String, Integer> cartItems = new HashMap<>();
        if (request.getCookies() != null) {
            getCartItemsMap(request, cartItems);
        }

        if (cartItems.size() == 0) {
            return new ResponseEntity<>("No products in the cart yet", HttpStatus.NOT_FOUND);
        }

        List<CartItem> cartItemList = new ArrayList<>();
        final double[] totalPrice = {0};

        cartItems.forEach((productId, quantity) -> {
            CustomProductForCart productById = productRepository.findProductById(UUID.fromString(productId));
            CartItem cartItem = new CartItem(productById, quantity);
            cartItemList.add(cartItem);
            totalPrice[0] += quantity * productById.getPrice();
        });
        double v = totalPrice[0];
        Cart cart = new Cart(cartItemList, v);

        return new ResponseEntity<>(cart, HttpStatus.OK);
    }


    public ResponseEntity<?> removeProductFromCart
            (UUID productId,
             HttpServletResponse response,
             HttpServletRequest request) {

        try {
            final String[] cartItemStr = {""};
            Map<String, Integer> cartItems = new HashMap<>();
            if (request.getCookies() != null) {
                getCartItemsMap(request, cartItems);
            }

            cartItems.remove(String.valueOf(productId));
            cartItems.forEach((key, value) -> cartItemStr[0] += key + "=" + value + "&");
            Cookie cookie = new Cookie("cartItems", cartItemStr[0]);
            cookie.setPath("/api");
//            cookie.setMaxAge(0);
            response.addCookie(cookie);
            return new ResponseEntity<>("deleted!", HttpStatus.OK);
        } catch (Exception ignored) {
        }
        return new ResponseEntity<>("Not deleted!", HttpStatus.CONFLICT);

    }


    public ResponseEntity<?> clearCart(HttpServletResponse response) {
        Cookie deletedCookie = new Cookie("cartItems", null);
        deletedCookie.setMaxAge(0);
        deletedCookie.setPath("/api");
//        Arrays.stream(request.getCookies()).forEach(cookie -> {
//            if (cookie.getName().equals("cartItems")) {
//                cookie.setMaxAge(0);
//                cookie.setValue(null);
//            }
//        });
        response.addCookie(deletedCookie);
        return new ResponseEntity<>("Successfully cleared!", HttpStatus.OK);

    }


    public void getCartItemsMap(HttpServletRequest request,
                                Map<String, Integer> map) {

        String cartItems = Arrays.stream(request.getCookies())
                .filter(c -> c.getName().equals("cartItems"))
                .findFirst()
                .map(Cookie::getValue)
                .orElse(null);

        if (cartItems != null) {
            for (String keyValue : cartItems.split(" *& *")) {
                String[] pairs = keyValue.split(" *= *", 2);
                map.put(pairs[0], pairs.length == 1 ? 1 : Integer.parseInt(pairs[1]));
            }
        }
    }
}

