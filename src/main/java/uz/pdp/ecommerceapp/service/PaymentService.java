package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 03/27/2022 12:53 PM


import com.stripe.exception.StripeException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.CartItem;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.projection.CustomProductForCart;
import uz.pdp.ecommerceapp.repository.ProductRepository;
import uz.pdp.ecommerceapp.repository.UserRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@RequiredArgsConstructor
@Service
public class PaymentService {

    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;


    private final CartService cartService;
    private final ProductRepository productRepository;
    private final StripePaymentService stripePaymentService;
    private final UserRepository userRepository;
    private final OrderService orderService;


    public ResponseEntity<?> purchaseProductsInTheCart(HttpServletRequest request, HttpServletResponse response) {

        Cookie cookie = new Cookie("phoneNumber", null);
        cookie.setMaxAge(0);
        cookie.setPath("/api");
        Cookie cookie1 = new Cookie("user", null);
        cookie1.setMaxAge(0);
        cookie1.setPath("/api");
        response.addCookie(cookie);
        response.addCookie(cookie1);


        User user = userRepository.findByEmail("sardorxoshimjanov");
        // TODO: 04/13/2022 get user from cookie

        Map<String, Integer> cartItems = new HashMap<>();
        cartService.getCartItemsMap(request, cartItems);

        if (cartItems.size() == 0) {
            return new ResponseEntity<>("No products in the cart yet", HttpStatus.NOT_FOUND);
        }

        List<CartItem> cartItemList = new ArrayList<>();

        cartItems.forEach((productId, quantity) -> {
            CustomProductForCart productById = productRepository
                    .findProductById(UUID.fromString(productId));
            CartItem cartItem = new CartItem(productById, quantity);
            cartItemList.add(cartItem);
        });
        try {
            cartService.clearCart(response);

            orderService.createNewOrderFromProductsInTheCart(cartItemList, user);

            return stripePaymentService.createStripeSession(cartItemList, user);

        } catch (StripeException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }


    public ResponseEntity<?> confirmPassword(Integer code,
                                             HttpServletResponse response,
                                             HttpServletRequest request) {


        Cookie cookie1 = Arrays.stream(request.getCookies())
                .filter(cookie ->
                        cookie.getName()
                                .equals("phoneNumber"))
                .findFirst().get();

        String phoneNumber = cookie1.getValue();
        User byPhoneNumber = userRepository.findByPhoneNumber(phoneNumber);

        if (Objects.equals(byPhoneNumber.getConfirmationCode(), code)) {
            Cookie cookie = new Cookie("user", byPhoneNumber.getId().toString());
            cookie.setPath("/api");
            response.addCookie(cookie);
            return purchaseProductsInTheCart(request, response);
        } else
            return new ResponseEntity<>("Wrong code!", HttpStatus.CONFLICT);
    }
}
