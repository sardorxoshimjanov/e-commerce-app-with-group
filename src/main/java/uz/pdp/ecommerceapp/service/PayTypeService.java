package uz.pdp.ecommerceapp.service;//@AllArgsConstructor

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.model.PayType;
import uz.pdp.ecommerceapp.repository.PayTypeRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

//@NoArgsConstructor
//@Data
//@Entity
//import static uz.sardor.Main.*;
//Sardor {4/12/2022}{ 4:02 AM}
@RequiredArgsConstructor
@Service
public class PayTypeService {

    private final PayTypeRepository payTypeRepository;

    public ResponseEntity<?> addPayType(PayType payType) {
        String payTypeName = payType.getName();
        for (PayType type : payTypeRepository.findAll()) {
            if (type.getName().equals(payTypeName))
                return new ResponseEntity<>("Category with this name already exists!", HttpStatus.CONFLICT);
        }
        PayType newPayType = new PayType();
        newPayType.setName(payType.getName());
        newPayType.setPercentage(payType.getPercentage());
        payTypeRepository.save(newPayType);
        return new ResponseEntity<>("Successfully saved!", HttpStatus.CREATED);
    }

    public ResponseEntity<?> getAllPayType() {
        List<PayType> payTypeList = payTypeRepository.findAll();
        System.out.println(payTypeList);
        if (!payTypeList.isEmpty()) {
            return ResponseEntity.ok(payTypeList);
        }
        return ResponseEntity.ok("PayType not found");
    }

    public ResponseEntity<?> deletePayTypeById(UUID id) {
        try {
            payTypeRepository.deleteById(id);
            return new ResponseEntity<>("Successfully deleted", HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Category doesn't deleted!", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> updatePayTypeById(UUID id, PayType payType) {
        Optional<PayType> optionalPayType = payTypeRepository.findById(id);
        if (optionalPayType.isPresent()) {
            PayType payType1 = optionalPayType.get();
            payType1.setName(payType.getName());
            payType1.setPercentage(payType.getPercentage());
            payTypeRepository.save(payType1);
            return new ResponseEntity<>("Successfully updated!", HttpStatus.OK);
        }
        return new ResponseEntity<>("Category with this id not found!", HttpStatus.NOT_FOUND);
    }

}
