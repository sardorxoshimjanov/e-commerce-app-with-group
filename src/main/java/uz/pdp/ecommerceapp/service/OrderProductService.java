package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/13/2022 5:20 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.projection.CustomOrderProduct;
import uz.pdp.ecommerceapp.repository.OrderProductRepository;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderProductService {


    private final OrderProductRepository orderProductRepository;


    public ResponseEntity<?> getAllOrderProducts(UUID orderId) {
        List<CustomOrderProduct> allOrderProductsOfOrderId = orderProductRepository.getAllOrderProductsOfOrderId(orderId);
        return ResponseEntity.ok(allOrderProductsOfOrderId);
    }
}
