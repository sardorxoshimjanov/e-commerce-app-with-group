package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/11/2022 9:49 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.CharacteristicDto;
import uz.pdp.ecommerceapp.model.Characteristic;
import uz.pdp.ecommerceapp.model.CharacteristicValue;
import uz.pdp.ecommerceapp.model.CharacteristicCategory;
import uz.pdp.ecommerceapp.model.Value;
import uz.pdp.ecommerceapp.projection.CustomCharacteristic;
import uz.pdp.ecommerceapp.repository.CharacteristicCategoryRepository;
import uz.pdp.ecommerceapp.repository.CharacteristicRepository;
import uz.pdp.ecommerceapp.repository.CharacteristicValueRepository;
import uz.pdp.ecommerceapp.repository.ValueRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class CharacteristicService {

    private final CharacteristicRepository characteristicRepository;
    private final CharacteristicCategoryRepository characteristicCategoryRepository;
    private final ValueRepository valueRepository;
    private final CharacteristicValueRepository characteristicValueRepository;


    public ResponseEntity<?> getAllCharacteristics() {
        List<CustomCharacteristic> characteristics = characteristicRepository.getAllCharacteristics();
        return ResponseEntity.ok(characteristics);
    }


    public ResponseEntity<?> addNewCharacteristicOrValuesToExistingCharacteristic(
            CharacteristicDto characteristicDto) {
        String name = characteristicDto.getName();
        Characteristic characteristic = characteristicRepository.findByName(name);
        UUID categoryId = characteristicDto.getCharacteristicsCategoryId();
        Optional<CharacteristicCategory> byId = characteristicCategoryRepository.findById(categoryId);

        if (!byId.isPresent()) {
            return new ResponseEntity<>("Category not found", HttpStatus.NOT_FOUND);
        }

        if (characteristic == null) {
            characteristic = characteristicRepository.save(
                    new Characteristic(name, characteristicDto.isMain(), byId.get()));
        }

        List<String> values = characteristicDto.getValues();
        for (String value : values) {
            Value byValue = valueRepository.findByValue(value);

            if (byValue != null) {
                CharacteristicValue characteristicValue = characteristicValueRepository.findCharacteristicValueByCharacteristicIdAndValueId(
                        characteristic.getId(), byValue.getId());
                if (characteristicValue != null) {
                    continue;
                }

            } else {
                byValue = valueRepository.save(new Value(value));
            }
            CharacteristicValue characteristicValue = new CharacteristicValue(characteristic, byValue);
            characteristicValueRepository.save(characteristicValue);
        }

        return new ResponseEntity<>("Successfully saved!", HttpStatus.CREATED);
    }


    public ResponseEntity<?> editCharacteristicById(
            UUID id,
            CharacteristicDto characteristicDto) {
        try {

            //finding characteristic category by id to check if exists
            Optional<CharacteristicCategory> categoryById = characteristicCategoryRepository
                    .findById(characteristicDto.getCharacteristicsCategoryId());


            if (!categoryById.isPresent()) {
                return new ResponseEntity<>("Category with this id does not exist", HttpStatus.NOT_FOUND);
            }


            //finding characteristic  by id to check if exists
            Optional<Characteristic> characteristicById = characteristicRepository.findById(id);

            if (!characteristicById.isPresent()) {
                return new ResponseEntity<>("Characteristic with this id does not exist", HttpStatus.NOT_FOUND);
            }
            //saving characteristic to database
            Characteristic characteristic = new Characteristic(characteristicDto.getName(), characteristicDto.isMain(), categoryById.get());
            characteristicRepository.save(characteristic);

            return new ResponseEntity<>("Successfully edited!", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Not edited!", HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<?> deleteCharacteristicById(UUID id) {
        try {
            characteristicRepository.deleteById(id);
            return new ResponseEntity<>("Successfully deleted", HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("You can't delete this characteristic", HttpStatus.CONFLICT);
    }


}
