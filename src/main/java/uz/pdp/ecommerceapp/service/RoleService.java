package uz.pdp.ecommerceapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.RoleDto;
import uz.pdp.ecommerceapp.model.Permission;
import uz.pdp.ecommerceapp.model.Role;
import uz.pdp.ecommerceapp.model.enums.RoleEnum;
import uz.pdp.ecommerceapp.payload.ApiResponse;
import uz.pdp.ecommerceapp.repository.PermissionRepository;
import uz.pdp.ecommerceapp.repository.RoleRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

// Bahodir Hasanov 4/14/2022 11:38 PM
@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PermissionRepository permissionRepository;

    public ApiResponse saveRole(RoleDto roleDto) {
        boolean b = roleRepository.existsByRole(RoleEnum.valueOf(roleDto.getName()));
        if (b) {
            return new ApiResponse("this role already exist", false);
        }
        Set<Permission> permissions = new HashSet<>();
        for (UUID uuid : roleDto.getPermissions()) {
            Optional<Permission> optionalPermission = permissionRepository.findById(uuid);
            if (optionalPermission.isPresent()) {
                permissions.add(optionalPermission.get());
            }
        }

        Role role = new Role(RoleEnum.valueOf(roleDto.getName()),permissions);
        roleRepository.save(role);
        return new ApiResponse("role has been created",true);

    }
}
