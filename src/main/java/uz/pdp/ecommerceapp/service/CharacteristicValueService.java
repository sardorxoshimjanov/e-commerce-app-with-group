package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/12/2022 10:28 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.CharacteristicValueDto;
import uz.pdp.ecommerceapp.model.CharacteristicValue;
import uz.pdp.ecommerceapp.projection.CustomCharacteristicValue;
import uz.pdp.ecommerceapp.repository.CharacteristicValueRepository;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class CharacteristicValueService {

    private final CharacteristicValueRepository characteristicValueRepository;


    public CharacteristicValue findCharacteristicValueIdByValueIdAndCharacteristicId(
            CharacteristicValueDto characteristicValueDto) {
        return characteristicValueRepository.findCharacteristicValueByCharacteristicIdAndValueId(
                characteristicValueDto.getCharacteristicId(), characteristicValueDto.getValueId()
        );
    }


    public ResponseEntity<?> deleteCharacteristicValueByValueIdAndCharacteristicId(
            CharacteristicValueDto characteristicValueDto) {

        CharacteristicValue characteristicValue = findCharacteristicValueIdByValueIdAndCharacteristicId(characteristicValueDto);
        try {
            characteristicValueRepository.deleteById(characteristicValue.getId());
            return new ResponseEntity<>("Successfully deleted!", HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Not deleted!", HttpStatus.CONFLICT);
    }


    public ResponseEntity<?> getAllValuesOfCharacteristic(UUID id) {
        List<CustomCharacteristicValue> allValues = characteristicValueRepository.findAllValues(id);
        return ResponseEntity.ok(allValues);
    }
}
