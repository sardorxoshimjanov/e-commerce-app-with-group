package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/13/2022 6:02 AM


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.model.Order;
import uz.pdp.ecommerceapp.model.PayType;
import uz.pdp.ecommerceapp.model.TransactionHistory;
import uz.pdp.ecommerceapp.projection.CustomTransHistory;
import uz.pdp.ecommerceapp.projection.CustomTransHistoryForUser;
import uz.pdp.ecommerceapp.repository.PayTypeRepository;
import uz.pdp.ecommerceapp.repository.TransactionHistoryRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TransactionHistoryService {


    private final PayTypeRepository payTypeRepository;
    private final TransactionHistoryRepository transactionHistoryRepository;


    public void saveTransactionHistory(Order order,
                                       double amountTotal,
                                       String paymentIntent) {
        PayType stripe = payTypeRepository.findByName("stripe");
        TransactionHistory transactionHistory = new TransactionHistory(order,
                stripe,
                paymentIntent);
        transactionHistoryRepository.save(transactionHistory);
    }

    public ResponseEntity<?> getAllTransactionHistory() {
        List<CustomTransHistory> customTransHistories = transactionHistoryRepository.showAllTransactionHistories();
        if (customTransHistories.isEmpty()) {
            return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customTransHistories, HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> getTransactionHistoryByUserId(HttpServletRequest request) {

        Cookie cookie1 = Arrays.stream(request.getCookies())
                .filter(cookie -> cookie
                        .getName()
                        .equals("user"))
                .findFirst().orElse(null);
        if (cookie1 == null) {
            return new ResponseEntity<>("Please,sign in first!", HttpStatus.OK);
        }

        String value = cookie1.getValue();
        UUID uuid = UUID.fromString(value);

        List<CustomTransHistoryForUser> customTransHistories = transactionHistoryRepository.showAllTransactionHistoryByUserId(uuid);

        if (customTransHistories.isEmpty()) {
            return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customTransHistories, HttpStatus.ACCEPTED);
    }
}
