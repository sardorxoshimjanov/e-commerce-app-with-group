package uz.pdp.ecommerceapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.model.Brand;
import uz.pdp.ecommerceapp.repository.BrandRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BrandService {

    private final BrandRepository brandRepository;

    public ResponseEntity<?> getAllBrands() {
        List<Brand> brandList = brandRepository.findAll();
        return ResponseEntity.ok(brandList);
    }

    public ResponseEntity<?> getBrandById(UUID brandId) {
        Optional<Brand> optionalBrand = brandRepository.findById(brandId);
        return ResponseEntity.status(optionalBrand.isPresent()?200:404).body(optionalBrand.get());
    }

    public ResponseEntity<?> editBrand(UUID brandId, Brand brand) {
        Optional<Brand> optionalBrand = brandRepository.findById(brandId);
        if (!optionalBrand.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Brand editingBrand = optionalBrand.get();
        editingBrand.setName(brand.getName());
        Brand editedBrand = brandRepository.save(editingBrand);
        return ResponseEntity.ok(editedBrand);
    }

    public ResponseEntity<?> addBrand(Brand brand) {
        Brand savedBrand = brandRepository.save(brand);
        return ResponseEntity.ok(savedBrand);
    }

    public ResponseEntity<?> deleteBrand(UUID brandId) {
        try {
            brandRepository.deleteById(brandId);
            return ResponseEntity.ok("successfully deleted");
        } catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.notFound().build();
    }
}
