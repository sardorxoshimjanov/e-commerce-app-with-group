package uz.pdp.ecommerceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

// Bahodir Hasanov 4/14/2022 3:00 PM
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {
    @NotNull(message = "please enter your full name")
    private String fullName;

    @NotNull(message = "please enter your phone number with code")
    private String phoneNumber;

    @NotNull
    @Email(message = "please enter your emil more")
    private String email;

    @NotNull(message = "please enter password")
    private String password;

    @NotNull(message = "please enter password one more")
    private String prePassword;




}
