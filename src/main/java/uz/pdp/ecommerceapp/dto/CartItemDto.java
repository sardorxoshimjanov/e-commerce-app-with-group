package uz.pdp.ecommerceapp.dto;
//Sevinch Abdisattorova 04/12/2022 12:49 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import java.util.UUID;


@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class CartItemDto {
    UUID productId;
    Integer quantity;
}
