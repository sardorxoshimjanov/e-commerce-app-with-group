package uz.pdp.ecommerceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

// Sardor Xoshuimjanov 4/11/2022 10:21 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class ProductRateDto {
  UUID userId;
  UUID productId;
  short rate;
}
