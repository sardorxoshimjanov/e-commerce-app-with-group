package uz.pdp.ecommerceapp.dto;
//Sevinch Abdisattorova 04/13/2022 11:48 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class UserDto {

    String fullName;
    String phoneNumber;

}
