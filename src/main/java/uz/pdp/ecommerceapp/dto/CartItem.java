package uz.pdp.ecommerceapp.dto;
//Sevinch Abdisattorova 04/12/2022 9:03 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.projection.CustomProductForCart;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@PackagePrivate
public class CartItem {
    CustomProductForCart product;
    Integer quantity;
}
