package uz.pdp.ecommerceapp.dto;
// Bahodir Hasanov 4/15/2022 1:17 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NotNull
@Data
public class AdminUserDto {
    @NotNull(message = "please enter your full name")
    private String fullName;

    @NotNull(message = "please enter your phone number with code")
    private String phoneNumber;

    @NotNull
    @Email(message = "please enter your emil more")
    private String email;

    @NotNull(message = "please enter password")
    private String password;

    @NotNull(message = "please enter password")
    private Set<UUID> rolesId;
}
