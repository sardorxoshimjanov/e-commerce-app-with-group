package uz.pdp.ecommerceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.validation.constraints.NotNull;
import java.util.UUID;


@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class CharacteristicValueDto {

    @NotNull
    UUID valueId;

    @NotNull
    UUID characteristicId;

}
