package uz.pdp.ecommerceapp.dto;
//Sevinch Abdisattorova 04/11/2022 10:25 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class CharacteristicDto {

    @NotNull
    String name;

    boolean isMain;

    @NotNull
    UUID characteristicsCategoryId;

    //for post mapping not for put mapping
    List<String> values;


}
