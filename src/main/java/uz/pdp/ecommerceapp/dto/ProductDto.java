package uz.pdp.ecommerceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 4/11/2022 10:21 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class ProductDto {
    @NotNull
    String name;
    UUID brandId;
    UUID categoryId;
    String description;
    @NotNull
    Double price;
    List<CharacteristicValueDto> characteristicValueDtoList;
}
