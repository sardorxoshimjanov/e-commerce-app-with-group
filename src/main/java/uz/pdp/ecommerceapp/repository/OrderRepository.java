package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.Order;
import uz.pdp.ecommerceapp.model.enums.Status;
import uz.pdp.ecommerceapp.projection.CustomOrder;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<Order, UUID> {

    Order findByUserIdAndStatus(UUID user_id, Status status);

    @Query(nativeQuery = true, value = "select cast(o.id as varchar) as id,\n" +
            "       cast(u.id as varchar) as userId,\n" +
            "       u.full_name           as userFullName,\n" +
            "       o.serial_number       as serialNumber,\n" +
            "       o.status              as status,\n" +
            "       o.created_at          as createdAt\n" +
            "from orders o\n" +
            "         join users u on u.id = o.user_id\n" +
            "order by o.created_at desc ")
    List<CustomOrder> getAllOrders();


    @Query(nativeQuery = true, value = "select cast(o.id as varchar) as id,\n" +
            "       cast(u.id as varchar) as userId,\n" +
            "       u.full_name           as userFullName,\n" +
            "       o.serial_number       as serialNumber,\n" +
            "       o.status              as status,\n" +
            "       o.created_at          as createdAt\n" +
            "from orders o\n" +
            "         join users u on u.id = o.user_id\n" +
            "where o.status='NEW'\n" +
            "order by o.created_at desc")
    List<CustomOrder> getAllNewOrders();

    @Query(nativeQuery = true, value = "select cast(o.id as varchar) as id,\n" +
            "       cast(u.id as varchar) as userId,\n" +
            "       u.full_name           as userFullName,\n" +
            "       o.serial_number       as serialNumber,\n" +
            "       o.status              as status,\n" +
            "       o.created_at          as createdAt\n" +
            "from orders o\n" +
            "         join users u on u.id = o.user_id\n" +
            "where u.id =:id\n" +
            "order by o.created_at desc")
    List<CustomOrder> getAllOrdersOfUser(UUID id);

    @Query(nativeQuery = true, value = "select serial_number " +
            "from orders " +
            "order by created_at desc " +
            "limit 1")
    Integer findLastSerialNumberOfOrder();
}
