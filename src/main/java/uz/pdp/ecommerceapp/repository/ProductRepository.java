package uz.pdp.ecommerceapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.ecommerceapp.model.Product;
import uz.pdp.ecommerceapp.projection.*;

import java.util.List;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {

    @Query(value = "select cast(p.id as varchar) as productId,\n" +
            "       p.name                as productName,\n" +
            "       c.name                as categoryName,\n" +
            "       p.price               as price\n" +
            "from products p\n" +
            "         join categories c on p.category_id = c.id where p.name ilike concat('%',:search,'%')", nativeQuery = true)
    Page<ProductProjection> findAllProductsByPage(Pageable pageable, @Param("search") String search);


    @Query(value = "select\n" +
            "       cast(id as varchar)      as productId,\n" +
            "       name                     as productName,\n" +
            "       price                    as productPrice,\n" +
            "       description              as description\n" +
            "from products\n" +
            "where id= :productId", nativeQuery = true)
    ProductByIdProjection getProductProjectionById(UUID productId);


    @Query(nativeQuery = true, value = "select cast(p2.id as varchar ), (select cast (a.id as varchar )\n" +
            "        from products p\n" +
            "                 join products_attachments pa on p.id = pa.product_id\n" +
            "                 join attachments a on a.id = pa.attachment_id\n" +
            "        Limit 1)\n" +
            "                as photoId,\n" +
            "       p2.name  as productName,\n" +
            "       p2.price as price\n" +
            "from products p2\n" +
            "where p2.id =:id\n")
    CustomProductForCart findProductById(UUID id);

    @Query(nativeQuery = true, value = "select cast(p.id as varchar)              as productId,\n" +
            "       concat('Characteristic: ', p.name) as productNameCharacteristic,\n" +
            "       cc.name                            as characteristicCategoryName\n" +
            "from products p\n" +
            "         join products_characteristics pc on p.id = pc.product_id\n" +
            "         join characteristics_values cv on pc.characteristics_value_id = cv.id\n" +
            "         join characteristics c on cv.characteristic_id = c.id\n" +
            "         join characteristics_categories cc on c.characteristic_category_id = cc.id\n" +
            "where pc.product_id = :productId")
    ProductWithCharProjection getProductWithCharacteristics(UUID productId);

    @Query(nativeQuery = true, value = "select pa.attachment_id from products_attachments pa where pa.product_id:productId")
    List<UUID> findAttachmentIdByProductId(UUID productId);


    @Query(nativeQuery = true, value = "select cast(p.id as varchar) as id,\n" +
            "       (select cast(pa.attachment_id as varchar ) " +
            "        from products p\n" +
            "                 join products_attachments pa on p.id = pa.product_id\n" +
            "        Limit 1) " +
            "                             as photoId,\n" +
            "       p.name                as name,\n" +
            "       p.price               as price\n" +
            "from orders o\n" +
            "         join order_items oi on o.id = oi.order_id\n" +
            "         join products p on p.id = oi.product_id\n" +
            "         join users u on u.id = o.user_id\n" +
            "where u.id = :id\n" +
            "  and o.status = 'NEW'")
    List<CustomProductForEmail> getProductsForEmailById(UUID id);


    @Query(nativeQuery = true, value = "select distinct category_id as categoryId,\n" +
            "                c.name      as categoryName,\n" +
            "                count(*)    as numberOfProducts\n" +
            "from products\n" +
            "         join categories c on c.id = products.category_id\n" +
            "where c.name ilike concat('%',:search,'%')" +
            "group by c.name, category_id")
    Page<CustomProductsCountByCategory> getProductsCountByCategory(Pageable pageable, String search);

    @Query(value = " select cast(p.id as varchar) as productId,\n" +
            "       p.name                as productName,\n" +
            "       c.name                as categoryName,\n" +
            "       p.price               as price\n" +
            "from products p\n" +
            "         join categories c on p.category_id = c.id " +
            "where c.id= :categoryId " +
            "and p.name ilike concat('%',:search,'%')", nativeQuery = true)
    Page<ProductProjection> getAllProductOfCategory(Pageable pageable, UUID categoryId, String search);


    @Query(nativeQuery = true, value = "select distinct cast(p.id as varchar) as productId,\n" +
            "                p.name                as productName,\n" +
            "                c.name                as categoryName,\n" +
            "                p.price               as price,\n" +
            "                count(*)              as numberOfUsersLiked\n" +
            "from products p\n" +
            "         join categories c on p.category_id = c.id\n" +
            "         join favorites_products fp on p.id = fp.product_id\n" +
            "where p.name ilike concat('%',:search,'%')" +
            "group by p.name, cast(p.id as varchar), c.name, p.price")
    Page<ProductProjectionForCountOfFavouriteProducts> getTheMostFavouriteProducts(Pageable pageable, String search);


}
