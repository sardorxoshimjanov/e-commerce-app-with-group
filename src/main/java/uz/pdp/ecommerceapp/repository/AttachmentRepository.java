package uz.pdp.ecommerceapp.repository;

import org.hibernate.annotations.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.ecommerceapp.model.Attachment;

import java.util.List;
import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {

    @Query(value = "select" +
            " cast(attachment_id as varchar)  " +
            "from products_attachments\n" +
            "where product_id = :productId", nativeQuery = true)
    List<UUID> getAttachmentByProductId(UUID productId);


    @Query(value = "select cast(attachment_id as varchar)\n" +
            "from products p\n" +
            "         join products_attachments pa\n" +
            "              on p.id = pa.product_id\n" +
            "         join attachments a on a.id = pa.attachment_id\n" +
            "where pa.product_id = :productId\n" +
            "order by a.created_at \n" +
            "limit 1", nativeQuery = true)
    UUID getOnlyOneAttachmentByProductId(UUID productId);

}
