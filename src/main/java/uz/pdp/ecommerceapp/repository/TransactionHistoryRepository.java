package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
//import org.w3c.dom.stylesheets.LinkStyle;
import uz.pdp.ecommerceapp.model.TransactionHistory;
import uz.pdp.ecommerceapp.projection.CustomTransHistory;
import uz.pdp.ecommerceapp.projection.CustomTransHistoryForUser;

import java.util.List;
import java.util.UUID;

@Repository
public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, UUID> {


    @Query(nativeQuery = true, value = "SELECT CAST(u.id AS varchar)  as userId,\n" +
            "       u.full_name            as userFullName,\n" +
            "       cast(pt.id as varchar) as payTypeId,\n" +
            "       pt.name                as payTypeName,\n" +
            "       cast(o.id as varchar)  as orderId,\n" +
            "       o.serial_number        as orderSerialNumber\n" +
            "FROM transaction_histories\n" +
            "         JOIN orders o on o.id = transaction_histories.order_id\n" +
            "         JOIN users u on u.id = o.user_id\n" +
            "         join pay_types pt on pt.id = transaction_histories.pay_type_id\n")
    List<CustomTransHistory> showAllTransactionHistories();


    @Query(nativeQuery = true, value = "SELECT " +
            "       cast(pt.id as varchar) as payTypeId,\n" +
            "       pt.name                as payTypeName,\n" +
            "       cast(o.id as varchar)  as orderId,\n" +
            "       o.serial_number        as orderSerialNumber\n" +
            "FROM transaction_histories\n" +
            "         JOIN orders o on o.id = transaction_histories.order_id\n" +
            "         JOIN users u on u.id = o.user_id\n" +
            "         join pay_types pt on pt.id = transaction_histories.pay_type_id\n")
    List<CustomTransHistoryForUser> showAllTransactionHistoryByUserId(UUID userId);
}
