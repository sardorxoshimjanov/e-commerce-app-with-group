package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.ecommerceapp.model.FirstPaymentPercentage;

import java.util.UUID;


public interface FirstPaymentPercentageRepository extends JpaRepository<FirstPaymentPercentage, UUID> {

}
