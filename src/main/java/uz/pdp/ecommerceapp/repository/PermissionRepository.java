package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.ecommerceapp.model.Permission;

import java.util.UUID;

public interface PermissionRepository extends JpaRepository<Permission, UUID> {
}
