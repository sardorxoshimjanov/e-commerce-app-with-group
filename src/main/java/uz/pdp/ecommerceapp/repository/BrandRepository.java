package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.ecommerceapp.model.Brand;

import java.util.UUID;


public interface BrandRepository extends JpaRepository<Brand,UUID> {

}
