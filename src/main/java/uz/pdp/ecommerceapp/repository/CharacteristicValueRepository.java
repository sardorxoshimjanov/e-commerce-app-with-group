package uz.pdp.ecommerceapp.repository;
//Sevinch Abdisattorova 04/11/2022 9:50 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.CharacteristicValue;
import uz.pdp.ecommerceapp.projection.CharacterAndValueNameProjection;
import uz.pdp.ecommerceapp.projection.CustomCharacteristicValue;

import java.util.List;
import java.util.UUID;

@Repository
public interface CharacteristicValueRepository extends JpaRepository<CharacteristicValue, UUID> {


    CharacteristicValue findCharacteristicValueByCharacteristicIdAndValueId(UUID characteristic_id, UUID value_id);

    @Query(nativeQuery = true, value = "select cast(values.id as varchar) , value\n" +
            "from values\n" +
            "         join characteristics_values cv on values.id = cv.value_id\n" +
            "where characteristic_id = :characteristicId")
    List<CustomCharacteristicValue> findAllValues(UUID characteristicId);

    @Query(nativeQuery = true, value = "select c.name  as characterName,\n" +
            "       v.value as valueName\n" +
            "from products_characteristics pc\n" +
            "         join characteristics_values cv on cv.id = pc.characteristics_value_id\n" +
            "         join characteristics c on c.id = cv.characteristic_id\n" +
            "         join values v on cv.value_id = v.id\n" +
            "where pc.product_id = :productId")
    List<CharacterAndValueNameProjection> getCharacterNameAndValueNameWithCharacterIdAndValueId(UUID productId);

}
