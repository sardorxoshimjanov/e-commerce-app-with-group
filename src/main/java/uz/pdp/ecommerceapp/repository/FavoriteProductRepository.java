package uz.pdp.ecommerceapp.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.FavoriteProduct;

import java.util.List;
import java.util.UUID;

@Repository
public interface FavoriteProductRepository extends JpaRepository<FavoriteProduct, UUID> {

    boolean existsByUserIdAndProductId(UUID userId, UUID productId);

    @Query(nativeQuery = true, value = "select id, created_at, updated_at, product_id, user_id from favorites_products " +
            "where favorites_products.user_id=:userId")
    List<FavoriteProduct> getAllFavoriteProductsByUserId(UUID userId);

    @Query(nativeQuery = true, value = "select * from favorites_products f " +
            "where f.product_id =:productId and f.user_id=:userId")
    FavoriteProduct  getFavoriteProductsByProduct(UUID productId, UUID userId);

    @Query(nativeQuery = true,value = "delete from favorites_products where favorites_products.id=:favoriteProductId")
    void deleteProduct(UUID favoriteProductId);

}
