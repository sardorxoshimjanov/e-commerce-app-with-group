package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.ecommerceapp.model.Role;
import uz.pdp.ecommerceapp.model.enums.RoleEnum;

import java.util.Set;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    Set<Role> findByRole(RoleEnum role);

    boolean existsByRole(RoleEnum role);



}
