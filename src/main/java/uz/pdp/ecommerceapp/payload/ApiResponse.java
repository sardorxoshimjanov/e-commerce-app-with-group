package uz.pdp.ecommerceapp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Bahodir Hasanov 3/15/2022 2:10 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApiResponse {
    private String message;
    private boolean status;
    private Object data;

    public ApiResponse(String message, boolean status) {
        this.message = message;
        this.status = status;
    }
}
