package uz.pdp.ecommerceapp.exception;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

// Bahodir Hasanov 4/14/2022 9:42 PM
@ResponseStatus(HttpStatus.NOT_FOUND)
@AllArgsConstructor
public class ResourceNotFoundException extends RuntimeException{

    private final String resourceName;//role
    private final String resourceField;//name
    private final Object field;//user,admin,1,500
}
