package uz.pdp.ecommerceapp.controller;
//Sevinch Abdisattorova 04/12/2022 12:32 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.dto.CartItemDto;
import uz.pdp.ecommerceapp.service.CartService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "api/cart")
public class CartController {

    private final CartService cartService;


    @PreAuthorize(value = "hasAuthority('GET_CART_ITEMS')")
    @GetMapping
    public ResponseEntity<?> getCartItems(
            HttpServletRequest request
    ) {
        return cartService.getCartItems(request);
    }


    @PreAuthorize(value = "hasAuthority('ADD_PRODUCT_TO_CART')")
    @PostMapping
    public ResponseEntity<?> addProductToCart(
            @RequestBody CartItemDto cartItemDto,
            HttpServletResponse response,
            HttpServletRequest request) {
        return cartService.addProductToCart(cartItemDto, request, response);
    }


    @PreAuthorize(value = "hasAuthority('CLEAR_CART')")
    @DeleteMapping
    public ResponseEntity<?> clearCart(
            HttpServletResponse response) {
        return cartService.clearCart(response);
    }


    @PreAuthorize(value = "hasAuthority('DELETE_PRODUCT_FROM_CART')")
    @DeleteMapping("/{productId}")
    public ResponseEntity<?> removeProductFromCart(
            @PathVariable UUID productId,
            HttpServletResponse response,
            HttpServletRequest request) {
        return cartService.removeProductFromCart(productId, response, request);
    }
}
