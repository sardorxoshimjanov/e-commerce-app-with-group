package uz.pdp.ecommerceapp.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.dto.ProductRateDto;
import uz.pdp.ecommerceapp.service.ProductRateService;
import uz.pdp.ecommerceapp.service.TransactionHistoryService;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/tarnsaction-history")
public class TransactionHistoryController {
    private final TransactionHistoryService transactionHistoryService;


    @GetMapping
    public ResponseEntity<?> showAllTransactionHistory() {
        return transactionHistoryService.getAllTransactionHistory();
    }

    @GetMapping("/user")
    public ResponseEntity<?> showAllTransactionHistoryByUserId(HttpServletRequest request) {
        return transactionHistoryService.getTransactionHistoryByUserId(request);
    }


}
