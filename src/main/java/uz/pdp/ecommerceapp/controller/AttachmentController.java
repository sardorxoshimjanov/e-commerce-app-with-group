package uz.pdp.ecommerceapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.ecommerceapp.service.AttachmentService;

import java.util.UUID;

// Bahodir Hasanov 4/14/2022 5:43 AM
@RestController
@RequestMapping("api/attachment")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @GetMapping("/{photoId}")
    public ResponseEntity<?> getPhotoById(@PathVariable UUID photoId) {
      return attachmentService.getPhotoById(photoId);
    }


}
