package uz.pdp.ecommerceapp.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.model.PayType;
import uz.pdp.ecommerceapp.service.PayTypeService;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/pay-type")
public class PayTypeController {

    private final PayTypeService payTypeService;


    @GetMapping
    public ResponseEntity<?> showAllPayType() {
        return payTypeService.getAllPayType();

    }


    @PostMapping
    public ResponseEntity<?> addPayType(@RequestBody PayType payType) {
        return payTypeService.addPayType(payType);
    }

    @DeleteMapping("/{payTypeId}")
    public ResponseEntity<?> deletePayType(@PathVariable UUID payTypeId) {
        return payTypeService.deletePayTypeById(payTypeId);
    }
    @PutMapping("/{payTypeId}")
    public ResponseEntity<?> updatePayType(@PathVariable UUID payTypeId,@RequestBody PayType payType) {
        return payTypeService.updatePayTypeById(payTypeId,payType);
    }


}
