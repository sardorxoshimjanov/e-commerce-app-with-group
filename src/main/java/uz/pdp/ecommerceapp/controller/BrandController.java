package uz.pdp.ecommerceapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.model.Brand;
import uz.pdp.ecommerceapp.service.BrandService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/brand")
public class BrandController {

    private final BrandService brandService;

    @GetMapping
    public ResponseEntity<?> getAllBrands(){
        return brandService.getAllBrands();
    }

    @GetMapping("/{brandId}")
    public ResponseEntity<?> getBrandById(@PathVariable UUID brandId){
        return brandService.getBrandById(brandId);
    }

    @PostMapping
    public ResponseEntity<?> addBrand(@RequestBody Brand brand){
        return brandService.addBrand(brand);
    }

    @PutMapping("/edit/{brandId}")
    public  ResponseEntity<?> addBrand(@PathVariable UUID brandId, @RequestBody Brand brand){
        return brandService.editBrand(brandId,brand);
    }

    @DeleteMapping("delete/{brandId}")
    public ResponseEntity<?> deleteBrand(@PathVariable UUID brandId){
        return brandService.deleteBrand(brandId);
    }


}
