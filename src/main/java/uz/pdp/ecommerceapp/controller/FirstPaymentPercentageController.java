package uz.pdp.ecommerceapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.model.FirstPaymentPercentage;
import uz.pdp.ecommerceapp.service.FirstPaymentPercentageService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/firstPaymentPercentage")
public class FirstPaymentPercentageController {

    private final FirstPaymentPercentageService firstPaymentPercentageService;

    @GetMapping
    public ResponseEntity<?> getAllFirstPaymentPercentages() {
        return firstPaymentPercentageService.getAllFirstPaymentPercentages();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getFirstPaymentPercentageById(@PathVariable UUID id) {
        return firstPaymentPercentageService.getFirstPaymentPercentageById(id);
    }

    @PostMapping
    public ResponseEntity<?> addFirstPaymentPercentage(@RequestBody FirstPaymentPercentage firstPaymentPercentage) {
        return firstPaymentPercentageService.addFirstPaymentPercentage(firstPaymentPercentage);
    }

    @PutMapping("/add/{id}")
    public ResponseEntity<?> addFirstPaymentPercentage(@PathVariable UUID id, @RequestBody FirstPaymentPercentage firstPaymentPercentage) {
        return firstPaymentPercentageService.editFirstPaymentPercentage(id, firstPaymentPercentage);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteFirstPaymentPercentage(@PathVariable UUID id) {
        return firstPaymentPercentageService.deleteFirstPaymentPercentage(id);
    }


}
