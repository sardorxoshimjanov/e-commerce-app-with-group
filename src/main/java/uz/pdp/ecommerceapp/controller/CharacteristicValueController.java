package uz.pdp.ecommerceapp.controller;
//Sevinch Abdisattorova 04/11/2022 9:46 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.dto.CharacteristicValueDto;
import uz.pdp.ecommerceapp.service.CharacteristicValueService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/characteristic-value")
public class CharacteristicValueController {


    private final CharacteristicValueService characteristicValueService;


    @PreAuthorize(value = "hasAuthority('GET_CHARACTERISTIC_VALUES')")
    @GetMapping("/{id}")
    public ResponseEntity<?> showAllValuesOfCharacteristic(
            @PathVariable UUID id) {
        return characteristicValueService.getAllValuesOfCharacteristic(id);
    }

    @PreAuthorize(value = "hasAuthority('DELETE_CHARACTERISTIC_VALUE')")
    @DeleteMapping
    public ResponseEntity<?> deleteCharacteristicValueByValueIdAndCharacteristicId(
            @RequestBody CharacteristicValueDto characteristicValueDto) {
        return characteristicValueService.deleteCharacteristicValueByValueIdAndCharacteristicId(
                characteristicValueDto
        );
    }
}
