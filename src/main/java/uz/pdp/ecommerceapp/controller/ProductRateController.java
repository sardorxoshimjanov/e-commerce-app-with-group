package uz.pdp.ecommerceapp.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.dto.ProductRateDto;
import uz.pdp.ecommerceapp.model.Category;
import uz.pdp.ecommerceapp.model.ProductRates;
import uz.pdp.ecommerceapp.repository.ProductRateRepository;
import uz.pdp.ecommerceapp.service.FavoriteProductService;
import uz.pdp.ecommerceapp.service.ProductRateService;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/product-rate")
public class ProductRateController {
    private final ProductRateService productRateService;

    @PostMapping
    public ResponseEntity<?> addNewProductRate(@RequestBody ProductRateDto productRateDto) {
        return productRateService.addProductRate(productRateDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProductRate(@PathVariable UUID id) {
        return productRateService.deleteProductRate(id);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<?> showProductRate(@PathVariable UUID productId) {
        return productRateService.getAllProductRateByProductId(productId);
    }

    @PutMapping()
    public ResponseEntity<?> updateProductRate(
            @RequestParam(value = "id") UUID id,
            @RequestParam(value = "rate") short rate) {
        return productRateService.updateProductRate(id, rate);
    }

}
