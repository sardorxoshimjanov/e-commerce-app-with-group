package uz.pdp.ecommerceapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.ecommerceapp.dto.LoginDto;
import uz.pdp.ecommerceapp.dto.RegisterDto;
import uz.pdp.ecommerceapp.dto.UserDto;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.payload.ApiResponse;
import uz.pdp.ecommerceapp.security.JwtProvider;
import uz.pdp.ecommerceapp.service.AuthService;

import javax.validation.Valid;

// Bahodir Hasanov 4/14/2022 2:55 PM
@RestController
@RequestMapping("api/auth")
public class AuthController {

    @Autowired
    AuthService authService;
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/register")
    public HttpEntity<?> registerUser(@Valid @RequestBody RegisterDto registerDto) {
        ApiResponse apiResponse = authService.registerUser(registerDto);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @PostMapping("/login")
    public HttpEntity<?> registerUser(@Valid @RequestBody LoginDto loginDto) {
        Authentication authenticate = authenticationManager.authenticate
                (new UsernamePasswordAuthenticationToken
                        (loginDto.getPhoneNumber(), loginDto.getPassword()));
        User user = (User) authenticate.getPrincipal();
        String token = jwtProvider.generateToken(user.getPhoneNumber(), user.getRoles());
        return ResponseEntity.ok("Bearer " + token);
    }
}
